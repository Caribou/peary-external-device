/**
 * Caribou Device implementation for MyPearyDevice
 */

#include <peary/hardware_abstraction/carboard/CarboardConstants.hpp>

#include "MyPearyDevice.hpp"

using namespace caribou;

MyPearyDevice::MyPearyDevice(const caribou::Configuration config)
    : CaribouDevice(config, iface_i2c::configuration_type(caribou::carboard::BUS_I2C2, 0x18)) {}


void MyPearyDevice::configure() {
  LOG(INFO) << "Configuring";
}
