/**
 * Caribou Device implementation for MyPearyDevice
 */

#pragma once

#include <peary/device/CaribouDevice.hpp>
#include <peary/hardware_abstraction/carboard/Carboard.hpp>

namespace caribou {

  /** MyPearyDevice Device class definition
   *
   *  this class implements the required functionality to operate MyPearyDevices via the
   *  Caribou device class interface.
   */
  class MyPearyDevice : public CaribouDevice<carboard::Carboard, iface_i2c> {

  public:
    MyPearyDevice(const caribou::Configuration config);
    ~MyPearyDevice();

    /** Initializer function for device
     */
    void configure() override;

    /** Turn on the power supply for the device
     */
    void powerUp() override;

    /** Turn off the device power
     */
    void powerDown() override;

    /** Start the data acquisition
     */
    void daqStart() override;

    /** Stop the data acquisition
     */
    void daqStop() override{};

  };

} // namespace caribou
